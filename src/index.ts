import log from 'loglevel';
import {
  MongoClient,
  Collection,
  FilterQuery,
  ObjectId,
  OptionalId,
} from 'mongodb';
import {
  IBaseCRUDStrService,
  RequiredId,
  jstr,
  IBaseEntity,
} from '@instants/core';

// ts-node src/client/client.ts

const uri = process.env.MONGO_URI as string;

const toMongoEntity = (entity: IBaseEntity<string>): IBaseEntity<string> & { _id: string } => ({...entity, _id: entity.id as string});

// const fromMongoEntity = (entity: { _id: string }): IBaseEntity<string> => ({...entity, id: entity._id});

// export const getMongoRepo = async (collectionName: string, uri: string) => {
//   log.info(1);
//   const client = new MongoClient(uri, {useNewUrlParser: true, useUnifiedTopology: true});

//   log.info(2);
//   await client.connect();

//   log.info(3);
//   const collection = client.db('test').collection(collectionName);

//   log.info(4);
// };

export class MongoRepo<T extends IBaseEntity<string>> implements IBaseCRUDStrService<T> {
  private readonly collectionName: string;

  private readonly uri: string;

  private collection?: Collection<T>;

  private client?: MongoClient;

  constructor(collectionName: string, uri: string) {
    this.collectionName = collectionName;
    this.uri = uri;
  }

  // [ECrudMethods.Get]: TGet<T>;
  // [ECrudMethods.Create]: TCreate<T, T>;
  // [ECrudMethods.Delete]: TDelete;
  // [ECrudMethods.ListWithPageInfo]: TList<TListRequest<T>, T>;
  // [ECrudMethods.Update]: TUpdate<T, T>;
  public async connect() {
    this.client = new MongoClient(this.uri, {useNewUrlParser: true, useUnifiedTopology: true});
    await this.client.connect();
    this.collection = this.client
      .db('test')
      .collection(this.collectionName);
  }

  public async close() {
    if (this.client) {
      await this.client.close();
    }
  }

  public async get(id: string): Promise<RequiredId<T> | null> {
    if (!this.collection) {
      throw new Error('Repo is not initialised');
    }

    // const result = (await this.collection.findOne(ObjectId.createFromHexString(id))) as RequiredId<T>;
    const result = (await this.collection.findOne({_id: ObjectId.createFromHexString(id)} as FilterQuery<T>));

    return {
      ...result,
      id: ((result as any)._id as ObjectId).toHexString(),
    } as RequiredId<T>;
  }

  // (id: number, webmasterId?: number) => Promise<RequiredId<T> | null>
  public async create(entity: T): Promise<RequiredId<T>> {
    if (!this.collection) {
      throw new Error('Repo is not initialised');
    }
    const resut = await this.collection.insertOne(toMongoEntity(entity) as OptionalId<any>);

    const id = resut.insertedId;

    return this.get(id.toHexString()) as unknown as RequiredId<T>;
  }

  // (entity: TCreate, fromUser?: boolean) => Promise<RequiredId<TGet>>
  public async delete(id: string) {
    if (!this.collection) {
      throw new Error('Repo is not initialised');
    }
    await this.collection.remove({_id: ObjectId.createFromHexString(id)});

    return id;
  }

  // (id: number) => Promise<number>
  public async update() {
    return {} as any;
  }

  // (entity: RequiredId<TUpdate>) => Promise<RequiredId<TGet>>
  public async list() {
    return {} as any;
  }

  // (request?: TForListRequest, fromUser?: boolean, userId?: number) => Promise<IList<RequiredId<TForList>>>
}

log.setDefaultLevel(log.levels.INFO);

export interface Test extends IBaseEntity<string> {
  name: string;
}

const app = async () => {
  log.info('start');
  log.info(`uri: ${uri}`);

  // const repo = await getMongoRepo('testTable', uri);

  const repo = new MongoRepo<Test>('testTable', uri);
  await repo.connect();
  log.info(5);

  const obj = {name: 'Alice'};

  const created = await repo.create(obj);
  log.info(`created: ${jstr(created)}`);

  log.info(`id: ${created.id}`);
  log.info(`id: ${ObjectId.createFromHexString(created.id)}`);
  log.info(`id: ${ObjectId.createFromHexString(created.id).toHexString()}`);

  const readed = await repo.get(created.id);
  log.info(`readed: ${jstr(readed)}`);

  log.info(6);
  await repo.close();
  log.info(7);
};

app();
